use wasm_bindgen::prelude::*;

use super::utils::set_panic_hook;

use theqatools_lib::cnpj;


#[wasm_bindgen]
pub fn generate_cnpj() -> String {
    set_panic_hook();
    cnpj::generate_cnpj()
}


#[wasm_bindgen]
pub fn generate_cnpj_for_branch(branch_no: &str) -> String {
    set_panic_hook();
    cnpj::generate_cnpj_for_branch(branch_no)
}


#[wasm_bindgen]
pub fn cnpj_is_valid(cnpj: &str) -> bool {
    set_panic_hook();
    cnpj::cnpj_is_valid(cnpj)
}
