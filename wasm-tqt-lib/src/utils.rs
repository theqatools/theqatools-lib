use wasm_bindgen::prelude::*;

use rand::{thread_rng, Rng};


pub fn set_panic_hook() {
    // When the `console_error_panic_hook` feature is enabled, we can call the
    // `set_panic_hook` function at least once during initialization, and then
    // we will get better error messages if our code ever panics.
    //
    // For more details see
    // https://github.com/rustwasm/console_error_panic_hook#readme
    #[cfg(feature = "console_error_panic_hook")]
    console_error_panic_hook::set_once();
}


#[wasm_bindgen]
pub fn next_i32(start: u32, end: u32) -> u32 {
    let mut rnd = thread_rng();
    rnd.gen_range(start, end)
}
