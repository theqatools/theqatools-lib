use std::convert::TryFrom;

use wasm_bindgen::prelude::*;

use super::utils::set_panic_hook;

use theqatools_lib::nfe::NFeData;


#[wasm_bindgen]
#[derive(Clone)]
pub struct NFe {
    yearMonth: String,
    issuerCnpj: String,
    pub uf: u8,
    pub model: u8,
    pub serie: u16,
    pub nfeNumber: u32,
    pub issueMode: u8,
    pub code: u32,
    pub dv: u8,
}


#[wasm_bindgen]
pub struct NFeKeyValidationResult {
    error_message: String,
    nfe: NFe,
    is_valid: bool,
}


#[wasm_bindgen]
impl NFeKeyValidationResult {
    #[wasm_bindgen(getter)]
    pub fn errorMessage(&self) -> String {
        self.error_message.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn nfe(&self) -> NFe {
        self.nfe.clone()
    }

    #[wasm_bindgen(getter)]
    pub fn isValid(&self) -> bool {
        self.is_valid
    }
}


#[wasm_bindgen]
impl NFe {
    pub fn new() -> NFe {
        set_panic_hook();

        NFe {
            uf: 0,
            model: 0,
            serie: 0,
            nfeNumber: 0,
            issueMode: 0,
            code: 0,
            dv: 0,
            yearMonth: String::new(),
            issuerCnpj: String::new(),
        }
    }

    #[wasm_bindgen(getter)]
    pub fn yearMonth(&self) -> String {
        self.yearMonth.clone()
    }

    #[wasm_bindgen(setter)]
    pub fn set_yearMonth(&mut self, yearMonth: String) {
        self.yearMonth = yearMonth;
    }

    #[wasm_bindgen(getter)]
    pub fn issuerCnpj(&self) -> String {
        self.issuerCnpj.clone()
    }

    #[wasm_bindgen(setter)]
    pub fn set_issuerCnpj(&mut self, issuerCnpj: String) {
        self.issuerCnpj = issuerCnpj;
    }
}


fn create_nfe_data(nfe: &NFe) -> NFeData {
    NFeData {
        uf_code: nfe.uf,
        year_month: &nfe.yearMonth,
        issuer_cnpj: &nfe.issuerCnpj,
        model: nfe.model,
        serie: nfe.serie,
        nfe_number: nfe.nfeNumber,
        issue_mode: nfe.issueMode,
        code: nfe.code
    }
}


fn create_nfe(nfe_data: &NFeData) -> NFe {
    NFe {
        uf: nfe_data.uf_code,
        yearMonth: nfe_data.year_month.to_string(),
        issuerCnpj: nfe_data.issuer_cnpj.to_string(),
        model: nfe_data.model,
        serie: nfe_data.serie,
        nfeNumber: nfe_data.nfe_number,
        issueMode: nfe_data.issue_mode,
        code: nfe_data.code,
        dv: nfe_data.dv()
    }
}


#[wasm_bindgen]
pub fn calculate_nfe_dv(nfe: &mut NFe) {
    set_panic_hook();

    let nfe_data = create_nfe_data(&nfe);
    nfe.dv = nfe_data.dv();
}


#[wasm_bindgen]
pub fn generate_nfe_key(nfe: NFe) -> String {
    set_panic_hook();

    let nfe_data = create_nfe_data(&nfe);
    nfe_data.key()
}


#[wasm_bindgen]
pub fn validate_nfe_key(key: &str) -> NFeKeyValidationResult {
    match NFeData::try_from(key) {
        Ok(nfe_data) => NFeKeyValidationResult {
            is_valid: true,
            nfe: create_nfe(&nfe_data),
            error_message: String::new(),
        }, 
        Err(error_message) => NFeKeyValidationResult {
            is_valid: false,
            nfe: NFe::new(),
            error_message: error_message.to_string(),
        }
    }
}