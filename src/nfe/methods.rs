use core::fmt::{Debug, Display};
use std::convert::TryFrom;
use std::str::FromStr;

use super::{FieldDefinition, NFeData, NFeField, ISSUE_MODES, NFE_DATA_SIZE, UF_CODES};
use crate::cnpj::cnpj_is_valid;
use crate::utils::{copy_to_u8_arr, has_only_base_10_numbers, mod11_dv};


fn uf_is_valid(uf_code: u8) -> bool {
    UF_CODES.iter().any(|&uf| uf == uf_code)
}


fn issue_mode_is_valid(issue_mode_code: u8) -> bool {
    ISSUE_MODES.iter().any(|&issue_mode| issue_mode == issue_mode_code)
}


impl NFeField {
    fn definition(&self) -> FieldDefinition {
        match self {
            NFeField::UF => FieldDefinition { index: 0, size: 2 },
            NFeField::YearMonth => FieldDefinition { index: 2, size: 4 },
            NFeField::IssuerCNPJ => FieldDefinition { index: 6, size: 14 },
            NFeField::Model => FieldDefinition { index: 20, size: 2 },
            NFeField::Serie => FieldDefinition { index: 22, size: 3 },
            NFeField::NFeNumber => FieldDefinition { index: 25, size: 9 },
            NFeField::IssuerMode => FieldDefinition { index: 34, size: 1 },
            NFeField::Code => FieldDefinition { index: 35, size: 8 },
        }
    }

    fn extract_key_part<'a>(&self, key: &'a str) -> &'a str {
        let definition = self.definition();
        &key[definition.index..definition.index + definition.size]
    }

    fn extract_from_key<T>(&self, key: &str) -> T
    where
        T: FromStr,
        T::Err: Debug,
    {
        self.extract_key_part(&key).parse::<T>().unwrap()
    }
}


impl<'a> NFeData<'a> {
    fn write_field<T>(&self, target_arr: &'a mut [u8], field: NFeField, value: &T)
    where
        T: Display,
    {
        let definition = field.definition();
        let mut arr = &mut target_arr[definition.index..definition.index + definition.size];
        match field {
            NFeField::UF | NFeField::Model => copy_to_u8_arr(&format!("{:0>2}", value), &mut arr),
            NFeField::Serie => copy_to_u8_arr(&format!("{:0>3}", value), &mut arr),
            NFeField::NFeNumber => copy_to_u8_arr(&format!("{:0>9}", value), &mut arr),
            NFeField::Code => copy_to_u8_arr(&format!("{:0>8}", value), &mut arr),
            NFeField::IssuerCNPJ => copy_to_u8_arr(&format!("{:0>14}", value), &mut arr),
            NFeField::YearMonth => copy_to_u8_arr(&format!("{:0>4}", value), &mut arr),
            NFeField::IssuerMode => copy_to_u8_arr(&format!("{:0>1}", value), &mut arr),
        };
    }

    pub fn numeric_repr(&self) -> [u8; NFE_DATA_SIZE] {
        let mut nfe_data: [u8; NFE_DATA_SIZE] = [0; NFE_DATA_SIZE];
        self.write_field(&mut nfe_data, NFeField::UF, &self.uf_code);
        self.write_field(&mut nfe_data, NFeField::YearMonth, &self.year_month);
        self.write_field(&mut nfe_data, NFeField::IssuerCNPJ, &self.issuer_cnpj);
        self.write_field(&mut nfe_data, NFeField::Model, &self.model);
        self.write_field(&mut nfe_data, NFeField::Serie, &self.serie);
        self.write_field(&mut nfe_data, NFeField::NFeNumber, &self.nfe_number);
        self.write_field(&mut nfe_data, NFeField::IssuerMode, &self.issue_mode);
        self.write_field(&mut nfe_data, NFeField::Code, &self.code);
        nfe_data
    }

    pub fn dv(&self) -> u8 {
        mod11_dv(&self.numeric_repr()) as u8
    }

    pub fn key(&self) -> String {
        let mut nfe_fields = self
            .numeric_repr()
            .iter()
            .map(|n| n.to_string())
            .collect::<String>();
        nfe_fields.push_str(&self.dv().to_string());
        nfe_fields
    }
}


impl<'a> TryFrom<&'a str> for NFeData<'a> {
    type Error = &'static str;

    fn try_from(key: &'a str) -> Result<Self, Self::Error> {
        if key.len() != NFE_DATA_SIZE + 1 {
            return Err("Invalid key size.");
        }

        if !has_only_base_10_numbers(key) {
            return Err("NFe key has invalid chars.");
        }

        let nfe_data = NFeData {
            uf_code: NFeField::UF.extract_from_key(&key),
            year_month: NFeField::YearMonth.extract_key_part(&key),
            issuer_cnpj: NFeField::IssuerCNPJ.extract_key_part(&key),
            model: NFeField::Model.extract_from_key(&key),
            serie: NFeField::Serie.extract_from_key(&key),
            nfe_number: NFeField::NFeNumber.extract_from_key(&key),
            issue_mode: NFeField::IssuerMode.extract_from_key(&key),
            code: NFeField::Code.extract_from_key(&key),
        };

        if !cnpj_is_valid(&nfe_data.issuer_cnpj) {
            return Err("Invalid CNPJ.");
        }

        if !uf_is_valid(nfe_data.uf_code) {
            return Err("Invalid UF.");
        }

        if !issue_mode_is_valid(nfe_data.issue_mode) {
            return Err("Invalid Issue Mode.");
        }

        if nfe_data.dv() != key.chars().last().unwrap().to_digit(10).unwrap() as u8 {
            return Err("Invalid DV.");
        }

        Ok(nfe_data)
    }
}
