mod methods;

const NFE_DATA_SIZE: usize = 0x2b;


const UF_CODES: [u8; 27] = [
    11, 12, 13, 14, 15, 16, 17, 21, 22, 23, 24,
    25, 26, 27, 28, 29, 31, 32, 33, 35, 41, 42,
    43, 50, 51, 52, 53,
];


const ISSUE_MODES: [u8; 5] = [1, 2, 3, 4, 5];


struct FieldDefinition {
    index: usize,
    size: usize,
}


enum NFeField {
    UF,
    YearMonth,
    IssuerCNPJ,
    Model,
    Serie,
    NFeNumber,
    IssuerMode,
    Code,
}


#[derive(Debug)]
pub struct NFeData<'a> {
    pub uf_code: u8,
    pub year_month: &'a str,
    pub issuer_cnpj: &'a str,
    pub model: u8,
    pub serie: u16,
    pub nfe_number: u32,
    pub issue_mode: u8,
    pub code: u32,
}


#[cfg(test)]
mod tests {
    use std::convert::TryFrom;

    use crate::nfe::NFeData;

    #[test]
    fn generate_nfe_key_test() {
        let nfe = NFeData {
            uf_code: 35,
            year_month: "1912",
            issuer_cnpj: "05555382000133",
            model: 55,
            serie: 3,
            nfe_number: 72551,
            issue_mode: 1,
            code: 31975193
        };
        assert_eq!(5, nfe.dv());

        let key = nfe.key();
        assert_eq!("35191205555382000133550030000725511319751935", key);
    }

    #[test]
    fn validate_nfe_key_test() {
        let nfe_data = NFeData::try_from("35191205555382000133550030000725511319751935");
        assert!(nfe_data.is_ok());
    }

    #[test]
    fn validate_nfe_key_size_error_test() {
        let nfe_data = NFeData::try_from("351912055553820001335500300007255113197519355");
        assert!(nfe_data.is_err());
        assert_eq!(nfe_data.unwrap_err(), "Invalid key size.");
    }

    #[test]
    fn validate_nfe_key_bad_char_error_test() {
        let nfe_data = NFeData::try_from("351912055553820001Z3550030000725511319751935");
        assert!(nfe_data.is_err());
        assert_eq!(nfe_data.unwrap_err(), "NFe key has invalid chars.");
    }

    #[test]
    fn validate_nfe_key_invalid_dv_error_test() {
        let nfe_data = NFeData::try_from("35191205555382000133550030000725511319751931");
        assert!(nfe_data.is_err());
        assert_eq!(nfe_data.unwrap_err(), "Invalid DV.");
    }
}
