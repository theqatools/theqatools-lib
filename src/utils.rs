pub(crate) fn copy_to_u8_arr(s: &str, arr: &mut [u8]) {
    if s.len() != arr.len() {
        panic!("origin ({}) and destination ({:?}) array has no the same length", s, arr);
    }
    for (i, c) in s.chars().enumerate() {
        arr[i] = c.to_digit(10).unwrap() as u8;
    }
}


pub(crate) fn mod11_dv(arr: &[u8]) -> u32 {
    let start = 2u32;
    let limit = 9u32;
    let mut current = start;
    let mut sum: u32 = 0;

    for i in (0..arr.len()).rev() {
        sum += arr[i] as u32 * current;
        current += 1;
        if current > limit {
            current = start;
        }
    }

    let module = sum % 11;

    if module == 0 || module == 1 {
        0
    } else {
        11 - module
    }    
}


pub(crate) fn has_only_base_10_numbers(s: &str) -> bool {
    s.len() > 0 && s.chars().all(|c| c.is_digit(10))
}
