// Com base na "Norma de Execução Conjunta Cief / CSAR nº 3, de 30 de janeiro de 1991"
// http://normas.receita.fazenda.gov.br/sijut2consulta/link.action?visao=anotado&idAto=20139 (acessado em 11/04/2020)


use rand::{thread_rng, Rng};

use crate::utils::{mod11_dv, has_only_base_10_numbers};


const CNPJ_SIZE: usize = 14;

const BRANCH_INDEX: usize = 8;

const BRANCH_SIZE: usize = 4;

const DV1_INDEX: usize = 12;

const DV2_INDEX: usize = 13;


fn fill_branch(cnpj_part: &mut [u8], branch_no: &str) {
    if branch_no.len() > BRANCH_SIZE {
        panic!("Invalid branch size.");
    }

    if !has_only_base_10_numbers(&branch_no) {
        panic!("Only numbers are allowed for branch.");
    }

    if branch_no.parse::<u16>().unwrap() == 0 {
        panic!("Invalid branch number.");
    }

    let formated_branch = format!("{:0>4}", branch_no);
    for (i, digit) in formated_branch.chars().enumerate() {
        cnpj_part[i] = digit.to_digit(10).unwrap() as u8;
    }
}


fn fill_registration_number(cnpj_part: &mut [u8]) {
    let mut thread_local_rnd = thread_rng();
    for i in 0..cnpj_part.len() {
        cnpj_part[i] = thread_local_rnd.gen_range(0, 9 + 1);
    }
}


fn build_cnpj_numbers_array(cnpj: &str) -> [u8; CNPJ_SIZE] {
    let mut cnpj_arr: [u8; CNPJ_SIZE] = [0; CNPJ_SIZE];
    for (i, value) in cnpj.chars().enumerate() {
        cnpj_arr[i] = value.to_digit(10).unwrap() as u8;
    }
    cnpj_arr
}


pub fn generate_cnpj() -> String {
    generate_cnpj_for_branch("1")
}


pub fn generate_cnpj_for_branch(branch_no: &str) -> String {
    let mut cnpj: [u8; CNPJ_SIZE] = [0; CNPJ_SIZE];
    fill_registration_number(&mut cnpj[..BRANCH_INDEX]);
    fill_branch(&mut cnpj[BRANCH_INDEX..BRANCH_INDEX + BRANCH_SIZE], branch_no);
    cnpj[DV1_INDEX] = mod11_dv(&cnpj[0..DV1_INDEX]) as u8;
    cnpj[DV2_INDEX] = mod11_dv(&cnpj[0..DV2_INDEX]) as u8;

    cnpj.iter()
        .map(|n| n.to_string())
        .collect::<String>()
}


pub fn cnpj_is_valid(cnpj: &str) -> bool {
    if cnpj.len() != CNPJ_SIZE || !has_only_base_10_numbers(&cnpj) {
        return false;
    }

    let cnpj = build_cnpj_numbers_array(&cnpj);
    let dv1 = mod11_dv(&cnpj[..DV1_INDEX]);
    let dv2 = mod11_dv(&cnpj[..DV2_INDEX]);

    cnpj[DV1_INDEX] as u32 == dv1 && cnpj[DV2_INDEX] as u32 == dv2
}


#[cfg(test)]
mod tests {
    use crate::cnpj::generate_cnpj_for_branch;
    use crate::cnpj::cnpj_is_valid;

    #[test]
    fn generate_cnpj_test() {
        for _ in 0..100 {
            let cnpj = generate_cnpj_for_branch("9999");
            assert!(cnpj_is_valid(&cnpj), "Invalid CNPJ generation {}", cnpj);
            assert_eq!("9999", &cnpj[8..12], "Invalid CNPJ generation {}", cnpj);
        }
    }

    #[test]
    #[should_panic]
    fn generation_error_branch_size_test() {
        generate_cnpj_for_branch("10000");
    }

    #[test]
    #[should_panic]
    fn generation_error_branch_format_test() {
        generate_cnpj_for_branch("z");
    }

    #[test]
    #[should_panic]
    fn generation_error_branch_number_test() {
        generate_cnpj_for_branch("0");
    }

    #[test]
    fn cnpj_is_valid_test() {
        let cnpjs = vec![
            "53658180000160",
            "30912494000141",
            "46109977000142",
            "72586186000113",
            "37399920000134",
            "27086288000115",
            "33732270000146",
            "32737003000107",
            "82667389000198",
            "64080440000189"
        ];

        for cnpj in cnpjs {
            assert!(cnpj_is_valid(cnpj), format!("Invalida CNPJ: {}", cnpj));
        }
    }

    #[test]
    fn cnpj_is_not_valid_test() {
        let cnpj = "1234567891234";
        assert!(!cnpj_is_valid(cnpj));
        let cnpj = "1234567891234a";
        assert!(!cnpj_is_valid(cnpj));
    }
}
